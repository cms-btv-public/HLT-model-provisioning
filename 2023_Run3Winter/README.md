# Model 2023 Run3Winter

The model is provided so PAGs can test its performance.
A presentation on this was given in this [trigger studies group meeting](https://indico.cern.ch/event/1326752/).

## Performance Improvement 

The performance improvement is shown in the ROC curves evaluated on TT and QCD samples.
The old model (CMSSW) that was trained on 2021 MC, is evaluated on the same samples as the 
Retraining.

![ROC comparison ttbar](roc_ttbar_bvsl.jpg){width=48%}
![ROC comparison qcd](roc_qcd_bvsl.jpg){width=48%}

## Loadinng the model in CMSSW

The model can be loaded into CMSSW by applying a simple customizer to the menu:

```python
# customizer.py
from importlib.machinery import SourceFileLoader
config = SourceFileLoader("config", "path/to/your/menu.pu").load_module()
process = config.process

process.hltPFDeepFlavourJetTags.model_path = cms.FileInPath("path/to/model.onnx")

```
and then running it with
```bash
cmsRun customizer.py
```

For a more integrated example see the [BTV@HLT tools](https://gitlab.cern.ch/cms-btv/hltupler/-/blob/ParticleNet/analyzers/HLT_btag_analyzer.py).

## Threshold tuning

Thresholds need to be tuned accoring to the working points.
These can be read off from the `.csv` files in the **working_points** directory.
They can be either translated by converting fixed mistagging rates or fixed efficiencies.

To do so, the mistagging/efficiency for the *old* threshold needs to be taken from the old `.csv` and then the new threshold needs to be taken from the new mistagging/efficiency.

![Old ttbar working point](working_point_old_ttbar.jpg){width=48%}
![New ttbar working point](working_point_new_ttbar.jpg){width=48%}
